{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

-- (MimeUnrender(mimeUnrender))
module Spec where

import Control.Concurrent.STM (TVar, modifyTVar, newTVarIO, readTVarIO)
import qualified Data.ByteString.Lazy as BL
import Data.CSV.Product
import qualified Data.Map as M
import Data.Time
import Generics.SOP
import Generics.SOP.TH
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Network.Wai
import Network.Wai.Handler.Warp
import Protolude hiding (toS)
import Protolude.Conv
import Servant.API
import Servant.CSV.Records
import Servant.Client
import Servant.Server
import System.IO.Unsafe (unsafePerformIO)
import Test.Hspec

--------------------- OneRecord  ---------------------------

-- data OneRecord

-- instance CSVMulti OneRecord where
--   type CSVIndex OneRecord = Int
--   type CSVProducts OneRecord = '[(Text, Int)]
--   csvMulti _ = T (defRT "index") $ L (RC $ defRT "name" :* defRT "age" :* Nil) E

data OneRecord' = OneRecord' Text Int deriving (Show, Eq)

deriveGeneric ''OneRecord'

instance CSVSingle OneRecord' where
  type CSVIndexSingle OneRecord' = Int
  csvSingle _ = defRT "index" :* defRT "name" :* defRT "age" :* Nil

type OneRecord = Single OneRecord'

oneRecord :: Proxy (CSV OneRecord x)
oneRecord = Proxy

ciao43 :: Tagged OneRecord [PutL OneRecord]
ciao43 = Tagged [lie $ OneRecord' "ciao" 43]

ciaoT12_43 :: Tagged OneRecord [UpdateL OneRecord]
ciaoT12_43 = Tagged [ti 12 $ lie $ OneRecord' "ciao" 43]

deleteIndex12 :: Tagged x [LD I Int]
deleteIndex12 = Tagged [tie 12]

--------------------- TwoRecord ---------------------------

--  a type index to express the encoding
data TwoRecord

--  a record
data Foo = Foo Int Text deriving (Show, Eq)

-- its SOP encoding
deriveGeneric ''Foo

-- a field encoding for UTCTime, this is how we add codec for columns
utcRT :: Text -> RT UTCTime
utcRT columnName = RT
  columnName
  do R $ toS . formatTime defaultTimeLocale "%Y-%m-%dT%H:%M:%S%QZ"
  do P $ parseTimeM True defaultTimeLocale "%Y-%m-%dT%H:%M:%S%QZ" . toS

-- another record
data Bar = Bar Text UTCTime deriving (Show, Eq)

-- its SOP encoding
deriveGeneric ''Bar

-- a type instance to make the encoding selecteable from servant mime typeclasses
instance CSVMulti TwoRecord where
  type CSVIndex TwoRecord = Int
  type CSVProducts TwoRecord = '[Bar, Foo]
  csvMulti _ =
    T (defRT "index2") $
      L (RC $ defRT "name" :* utcRT "date" :* Nil) $
        L (RC $ defRT "feet number" :* defRT "secret PIN" :* Nil) E

twoRecord :: Proxy (CSV TwoRecord x)
twoRecord = Proxy

{-# NOINLINE now #-}
now :: UTCTime
now = unsafePerformIO getCurrentTime

nowText :: BL.ByteString
nowText = toS . formatTime defaultTimeLocale "%Y-%m-%dT%H:%M:%S%QZ" $ now

ciao_now_72_1234 :: Tagged TwoRecord [PutL TwoRecord]
ciao_now_72_1234 = Tagged [li (Bar "ciao" now) $ lie (Foo 72 "1234")]

ciao_T42_now_72_1234 :: Tagged TwoRecord [QueryL TwoRecord]
ciao_T42_now_72_1234 = Tagged [ti 42 $ li (Bar "ciao" now) $ lie (Foo 72 "1234")]

--------------------- servant API def ---------------------------

type TestApi l =
  Get '[CSV l 'Querying] (Tagged l [QueryL l])
    :<|> ReqBody '[CSV l 'Putting] (Tagged l [PutL l]) :> Put '[PlainText] NoContent
    :<|> ReqBody '[CSV l 'Updating] (Tagged l [UpdateL l]) :> Patch '[PlainText] NoContent
    :<|> ReqBody '[CSV l 'Deleting] (Tagged l [DeleteL l]) :> DeleteNoContent

testApi :: Proxy (TestApi l)
testApi = Proxy

-- a minimal state for rows
data RamState l = RamState
  { _ramState_state :: Map (CSVIndex l) (PutL l)
  , _ramState_nextID :: CSVIndex l
  }


insertRecords :: (Ord (CSVIndex l), Enum (CSVIndex l)) => [PutL l] -> RamState l -> RamState l
insertRecords xs s = foldl' insertRecord s xs

insertRecord :: (Ord (CSVIndex l), Enum (CSVIndex l)) => RamState l -> PutL l -> RamState l
insertRecord (RamState state' index') x = RamState (M.insert index' x state') (succ index')

updateRecords :: (Ord (CSVIndex l), Enum (CSVIndex l)) => [UpdateL l] -> RamState l -> RamState l
updateRecords xs s = foldl' updateRecord s xs

updateRecord :: Ord (CSVIndex l) => RamState l -> UpdateL l -> RamState l
updateRecord (RamState state' index') (T (I i) x) = RamState (M.insert i x state') index'

listRecords :: (Eq (CSVIndex l), IsUpdateOp (PutOpOf (CSVProducts l)) ~ False) => RamState l -> [QueryL l]
listRecords (RamState state' _) = uncurry (T . I) <$> M.assocs state'

bootState :: (Ord (CSVIndex l), Enum (CSVIndex l), IsUpdateOp (PutOpOf (CSVProducts l)) ~ False) => Tagged l [QueryL l] -> RamState l
bootState (Tagged xs) = RamState
  do M.fromList $ (\(T (I i) l) -> (i, l)) <$> xs
  do succ $ maximum $ unI . getIndex <$> xs

server :: (Enum (CSVIndex l), Ord (CSVIndex l), Show (CSVIndex l), IsUpdateOp (PutOpOf (CSVProducts l)) ~ False) => TVar (RamState l) -> Server (TestApi l)
server stateT = getA :<|> putS :<|> updateS :<|> deleteS
  where
    getA = fmap (Tagged . listRecords) $ liftIO $ readTVarIO stateT
    putS (Tagged xs) = do
      liftIO $ atomically $ modifyTVar stateT (insertRecords xs)
      pure NoContent
    updateS (Tagged xs) = do
      liftIO $ atomically $ modifyTVar stateT (updateRecords xs)
      pure NoContent
    deleteS (Tagged xs) = do
      liftIO $ atomically $ modifyTVar stateT (deleteRecords xs)
      pure NoContent

deleteRecords :: Ord (CSVIndex l) => [LD I (CSVIndex l)] -> RamState l -> RamState l
deleteRecords xs s = foldl' deleteRecord s xs

deleteRecord :: Ord (CSVIndex l) => RamState l -> LD I (CSVIndex l) -> RamState l
deleteRecord (RamState state' index') (T (I i) E) = RamState (M.delete i state') index'

queryL :: CSVMulti l => ClientM (Tagged l [QueryL l])
putL :: CSVMulti l => Tagged l [PutL l] -> ClientM NoContent
updateL :: CSVMulti l => Tagged l [UpdateL l] -> ClientM NoContent
deleteL :: CSVMulti l => Tagged l [LD I (CSVIndex l)] -> ClientM NoContent
queryL :<|> putL :<|> updateL :<|> deleteL = client testApi

application
  :: (Ord (CSVIndex l), Enum (CSVIndex l), CSVMulti l, Show (CSVIndex l))
  => TVar (RamState l)
  -> Application
application stateT req resp = do
  serve testApi (server stateT) req resp

testServer
  :: (Ord (CSVIndex l), Enum (CSVIndex l), CSVMulti l, Show (CSVIndex l))
  => Tagged l [QueryL l]
  -> ClientM (Tagged l [QueryL l])
  -> IO (Tagged l [QueryL l])
testServer input testA = do
  resultV <- newEmptyMVar
  stateT <- liftIO $ newTVarIO $ bootState input
  let test = do
        manager' <- newManager defaultManagerSettings
        void $
          forkIO $ do
            res <- runClientM testA $ mkClientEnv manager' $ BaseUrl Http "localhost" 3000 ""
            case res of
              Left err -> panic $ show err
              Right result -> putMVar resultV result
      settings = setBeforeMainLoop test (setPort 3000 defaultSettings)
  stop <- forkIO $ runSettings settings $ application stateT
  output <- takeMVar resultV
  killThread stop
  threadDelay 100_000
  pure output

spec_render :: Spec
spec_render = do
  describe "one records" $ do
    it "renders putting one value" $ shouldBe
      do mimeRender (oneRecord @'Putting) ciao43
      do "name,age\nciao,43\n"
    it "renders putting no values" $ shouldBe
      do mimeRender (oneRecord @'Putting) (Tagged @OneRecord ([] @(PutL OneRecord)))
      do "name,age\n"
    it "parses querying rows" $ shouldBe
      do mimeUnrender (oneRecord @'Querying) "index,name,age\n12,ciao,43\n"
      do Right ciaoT12_43
    it "parses querying no rows" $ shouldBe
      do mimeUnrender (oneRecord @'Querying) "index,name,age\n"
      do Right (Tagged @OneRecord ([] @(QueryL OneRecord)))
    it "renders updating one row" $ shouldBe
      do mimeRender (oneRecord @'Updating) ciaoT12_43
      do "index,name,age\n12,ciao,43\n"
    it "renders deleting one row" $ shouldBe
      do mimeRender (oneRecord @'Deleting) (deleteIndex12 @OneRecord)
      do "index\n12\n"
  describe "two records" $ do
    it "renders putting one value" $ shouldBe
      do mimeRender (twoRecord @'Putting) ciao_now_72_1234
      do "name,date,feet number,secret PIN\nciao," <> nowText <> ",72,1234\n"
    it "parses querying rows" $
      shouldBe
        do
          mimeUnrender
            (twoRecord @'Querying)
            do "index2,name,date,feet number,secret PIN\n12,ciao," <> nowText <> ",72,1234\n"
        do Right (fmap (fmap $ T (I 12)) ciao_now_72_1234)
    it "renders updating one row" $
      shouldBe
        do mimeRender (twoRecord @'Updating) ciao_T42_now_72_1234
        do "index2,name,date,feet number,secret PIN\n42,ciao," <> nowText <> ",72,1234\n"
    it "renders deleting one row" $
      mimeRender (twoRecord @'Deleting) (deleteIndex12 @TwoRecord) == "index2\n12\n"
  describe "Querying API" $ do
    it "serve a Oneecord" $ do
      r <- testServer ciaoT12_43 queryL
      shouldBe r ciaoT12_43
    it "serve a TwoRecord" $ do
      r <- testServer ciao_T42_now_72_1234 queryL
      shouldBe r ciao_T42_now_72_1234
  describe "Querying API" $ do
    it "serve a OneRecord" $ do
      r <- testServer ciaoT12_43 queryL
      shouldBe r ciaoT12_43
    it "serve a TwoRecord" $ do
      r <- testServer ciao_T42_now_72_1234 queryL
      shouldBe r ciao_T42_now_72_1234
  describe "Putting API" $ do
    it "put a OneRecord and get it back" $ do
      r <- testServer ciaoT12_43 $ putL ciao43 >> queryL
      shouldBe r $ Tagged
        do
          [ ti 12 $ lie $ OneRecord' "ciao" 43
            , ti 13 $ lie $ OneRecord' "ciao" 43
            ]
    it "put a TwoRecord and get it back" $ do
      r <- testServer ciao_T42_now_72_1234 $ putL ciao_now_72_1234 >> queryL
      shouldBe r $ Tagged
        do
          [ ti 42 $ li (Bar "ciao" now) $ lie (Foo 72 "1234")
            , ti 43 $ li (Bar "ciao" now) $ lie (Foo 72 "1234")
            ]
  describe "Updating API" $ do
    it "update a OneRecord and get it back" $ do
      r <-
        testServer ciaoT12_43 $
          updateL (Tagged @OneRecord [T (I 12) $ lie $ OneRecord' "ciao" 47]) >> queryL
      shouldBe r $ Tagged
        do [ti 12 $ lie $ OneRecord' "ciao" 47]
  describe "Delete API" $ do
    it "delete a OneRecord and get back null" $ do
      r <-
        testServer ciaoT12_43 $
          deleteL (Tagged @OneRecord [tie 12]) >> queryL
      shouldBe r $ Tagged
        do []
