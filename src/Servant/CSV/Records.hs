{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE UndecidableSuperClasses #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Servant.CSV.Records where

import Data.ByteString.Lazy (ByteString)
import Data.CSV.Product
    ( LU,
      LP,
      recordColumns,
      parseProducts,
      renderProducts,
      getDeleteOp,
      DeleteOp,
      UpdateOpOf,
      PutOpOf,
      LD,
      IsUpdateOp,
      Op(UpdateOp),
      L(T, L, E, getPutOp),
      IsRecord,
      RC(RC),
      RT )
import Data.Csv
    ( decodeByNameWithP,
      defaultEncodeOptions,
      encodeWith,
      defaultDecodeOptions,
      EncodeOptions(encUseCrLf),
      Record )
import Data.Tagged ( Tagged(..) )
import GHC.Exts ( IsList( fromList), Char )
import Generics.SOP ( All, I, IsProductType, NP((:*)), Proxy(..), Code, ProductCode )
import qualified Network.HTTP.Media as M
import Protolude hiding (toS, ByteString, All)
import Protolude.Conv ( toS )
import Servant.API
  ( Accept (..)
  , MimeRender (..)
  , MimeUnrender (..)
  )

-- | supported operations
data Operation = Querying | Updating | Deleting | Putting

-- | mime at type level
-- * 'l' is an encoding index type
-- * 'a' is the operaation we are aiming at
data CSV l (a :: Operation)

instance Accept (CSV l x) where
  contentType _ = "text" M.// "csv"

class
  ( All IsRecord (CSVProducts l)
  , IsUpdateOp (PutOpOf (CSVProducts l)) ~ 'False
  ) =>
  CSVMulti l
  where
  type CSVIndex l
  type CSVProducts l :: [*]
  csvMulti :: Proxy l -> L (UpdateOpOf (CSVProducts l)) (RC RT) (CSVIndex l) (CSVProducts l)

proxyL :: Proxy (CSV l a) -> Proxy l
proxyL _ = Proxy

type MimeC l u o op = (CSVIndex l ~ o, CSVProducts l ~ u, CSVMulti l, PutOpOf u ~ op)

class CSVSingle a where
  type CSVIndexSingle a :: *
  csvSingle :: Proxy a -> NP RT (CSVIndexSingle a ': ProductCode a)

data Single a

instance
  ( CSVSingle a
  , IsRecord a
  {- , Show a
  , Eq a
  , Eq (CSVIndexSingle a) -}
  )
  => CSVMulti (Single a)
  where
  type CSVIndex (Single a) = CSVIndexSingle a
  type CSVProducts (Single a) = '[a]
  csvMulti _ =
    let t :* rs = csvSingle (Proxy @a)
     in T t $ L (RC rs) E

--------------------- mime unrender instances ---------------------------

mimeUnrender' :: L op (RC RT) o u -> ByteString -> Either [Char] [L op I o u]
mimeUnrender' lrt =
  fmap (toList . snd) . decodeByNameWithP
    do parseProducts lrt
    do defaultDecodeOptions

instance MimeC l u o op => MimeUnrender (CSV l 'Querying) (Tagged l [L ('UpdateOp op) I o u]) where
  mimeUnrender (csvMulti . proxyL -> lrt) x = Tagged @l <$> mimeUnrender' lrt x

instance MimeC l u o op => MimeUnrender (CSV l 'Putting) (Tagged l [L op I o u]) where
  mimeUnrender (getPutOp . csvMulti . proxyL -> lrt) x = Tagged @l <$> mimeUnrender' lrt x

instance MimeC l u o op => MimeUnrender (CSV l 'Updating) (Tagged l [L ('UpdateOp op) I o u]) where
  mimeUnrender (csvMulti . proxyL -> lrt) x = Tagged @l <$> mimeUnrender' lrt x

instance MimeC l u o op => MimeUnrender (CSV l 'Deleting) (Tagged l [L DeleteOp I o '[]]) where
  mimeUnrender (getDeleteOp . csvMulti . proxyL -> lrt) x = Tagged @l <$> mimeUnrender' lrt x

--------------------- mime render instances ---------------------------

mimeRender' :: L op (RC RT) o a -> [L op I o a] -> ByteString
mimeRender' lrt xs =
  encodeWith @Record (defaultEncodeOptions {encUseCrLf = False}) $
    fmap fromList $
      (toS <$> recordColumns lrt) : (renderProducts lrt <$> toList xs)

instance MimeC l u o op => MimeRender (CSV l 'Querying) (Tagged l [L ('UpdateOp op) I o u]) where
  mimeRender (csvMulti . proxyL -> lrt) = mimeRender' lrt . unTagged

instance MimeC l u o op => MimeRender (CSV l 'Putting) (Tagged l [L op I o u]) where
  mimeRender (getPutOp . csvMulti . proxyL -> lrt) = mimeRender' lrt . unTagged

instance MimeC l u o op => MimeRender (CSV l 'Updating) (Tagged l [L ('UpdateOp op) I o u]) where
  mimeRender (csvMulti . proxyL -> lrt) = mimeRender' lrt . unTagged

instance MimeC l u o op => MimeRender (CSV l 'Deleting) (Tagged l [L DeleteOp I o '[]]) where
  mimeRender (getDeleteOp . csvMulti . proxyL -> lrt) = mimeRender' lrt . unTagged

--------------------- supported mime types data ---------------------------

-- | type that can be queried
type QueryL l = LU I (CSVIndex l) (CSVProducts l)

-- | type  that can be inserted
type PutL l = LP I (CSVIndex l) (CSVProducts l)

-- | type that can be updated (same as queried)
type UpdateL l = QueryL l

-- |  type for record deletion
type DeleteL l = LD I (CSVIndex l)
