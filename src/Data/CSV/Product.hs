{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE UndecidableSuperClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}

module Data.CSV.Product where

import Data.Csv
    ( (.:), NamedRecord, FromField(..), ToField(..), Parser, Field )
import Generics.SOP
    ( productTypeTo,
      productTypeFrom,
      hmap,
      All,
      Top,
      HCollapse(hcollapse),
      K(K),
      I(..),
      IsProductType,
      NP(..),
      ProductCode, Generic (Code) )
import Protolude hiding (All, ByteString, toList, toS)
import Protolude.Conv ( toS )
import Unsafe.Coerce (unsafeCoerce)
import Data.Functor.Invariant ( Invariant(..) )
import Generics.SOP.Constraint (Head)
import qualified Generics.SOP as SOP

--------------------- column level CSV operations ---------------------------

-- | render to 'Field'
newtype R a = R {unR :: a -> Field}

-- | parser from 'Field'
newtype P a = P {unP :: Field -> Parser a} deriving (Functor)

-- | a column level parse and render with column name
-- should be named RP
data RT a = RT
  { -- | column name
    rtColumn :: Text
  , -- | how to render 'a'
    rtRender :: R a
  , -- | how to parse 'a'
    rtParse :: P a
  }

instance Invariant RT where 
  invmap f g (RT c (R r) p) = RT c (R $ r . g) (f <$> p)
  
-- | default RT, it uses instances from Cassava FromField and ToField
defRT :: (ToField a, FromField a) => Text -> RT a
defRT c = RT c (R toField) (P parseField)


-- | capture the SOP product type of 'a'
newtype RC f a = RC {unRC :: NP f (ProductCode a)}

type family Output a where
  Output (RC RT) = RT
  Output I = I

-- | type level counting Op
data Op where
  NoOp :: Op
  PutOp :: Op -> Op
  UpdateOp :: Op -> Op

type DeleteOp = 'UpdateOp 'NoOp

-- | capture the product type encoding

type IsRecordC a = (IsProductType a (ProductCode a), All Top (ProductCode a))

class IsRecordC a => IsRecord a

instance (IsRecordC a) => IsRecord a


{-
T is an optional first element indicating the id of type o
L is Cons indicating other data
E is Nil

T is optional, because for creation it does not exist yet
Ls will be omitted for deletion

   put: L (L E)         (type: L 'PutOp ... [productType1, productType2])
delete: T E             (type: L 'NoOp ... [])
update: T (L (L (E))    (type: L 'UpdateOp ... [productType1, productType2]))  
   get: T (L (L (E))    (type: L 'UpdateOp ... [productType1, productType2])  
-}

-- | list of heterogeneous product types
-- * 'f' is a context for their fields
-- * 'o' is the type to index L elements
-- * 'a' is a list of product types
data L (op :: Op) f o (a :: [*]) where
  T
    :: (IsUpdateOp op ~ 'False) =>
    { getIndex :: Output f o
    , getPutOp :: L op f o b
    } ->
    L ('UpdateOp op) f o b
  L
    -- :: (IsRecord a) =>
    -- :: (IsProductType a (ProductCode a)) =>
    -- :: (Generics.SOP.Generic a, Code a ~ '[ProductCode a]) =>
    -- :: (Generics.SOP.Generic a, Code a ~ '[Head (Code a) ]) =>
    :: (IsRecordC a) =>
    { getRecord :: f a
    , getRest :: L op f o b
    } ->
    L ('PutOp op) f o (a : b)
  E :: L 'NoOp f o '[]

deriving instance (All Show a, Show o) => Show (L op I o a)
deriving instance (All Eq a, Eq o) => Eq (L op I o a)


-- | uncons an 'L' which starts with 'T'
getIndexed :: L ('UpdateOp op) f o a -> (Output f o, L op f o a)
getIndexed (T i x) = (i, x)

-- | change index of 'L'
reT ::  Output f o' -> L op f o b -> L op f o' b
reT x (T _y a) = T x $ unsafeCoerce a
reT _w a = unsafeCoerce a



-- | compute the 'Op' from the list of product types
type family PutOpOf a where
  PutOpOf '[] = 'NoOp
  PutOpOf (x : xs) = 'PutOp (PutOpOf xs)

type UpdateOpOf a = 'UpdateOp (PutOpOf a)

type family IsUpdateOp x where
  IsUpdateOp ('UpdateOp op) = 'True
  IsUpdateOp _ = 'False

-- | remove all PutOp layers
getDeleteOp :: L ('UpdateOp op) f o a -> L ('UpdateOp 'NoOp) f o '[]
getDeleteOp (T x _) = T x E

--------------------- rendering ---------------------------

-- | render all product types in L
renderProducts :: L op (RC RT) o a -> L op I o a -> [Field]
renderProducts E E = []
renderProducts (L (RC rts) rtss) (L (I x) xs) =
  renderProduct (hmap rtRender rts) (productTypeFrom x) <> renderProducts rtss xs
renderProducts (T RT {..} rtss) (T (I x) xs) = unR rtRender x : renderProducts rtss xs

-- | render one product type
renderProduct :: NP R as -> NP I as -> [Field]
renderProduct Nil Nil = []
renderProduct (R f :* fs) (I x :* xs) =
  f x : renderProduct fs xs

--------------------- parsing ---------------------------

-- | parse all product types in L
parseProducts :: L op (RC RT) o a -> NamedRecord -> Parser (L op I o a)
parseProducts E _ = pure E
parseProducts (L (RC rts) rest) r = do
  s' <- parseProduct rts r
  rest' <- parseProducts rest r
  pure $ L (I $ productTypeTo s') rest'
parseProducts (T RT {..} rest) r = do
  s' <- r .: toS rtColumn >>= unP rtParse
  rest' <- parseProducts rest r
  pure $ T (I s') rest'

parseProduct :: NP RT a -> NamedRecord -> Parser (NP I a)
parseProduct Nil _ = pure Nil
parseProduct (RT c _ (P p) :* ps) r = do
  s <- parseProduct ps r
  x <- r .: toS c >>= p
  pure $ I x :* s

--------------------- select columns ---------------------------

-- | columns of a product type
productColumns :: All Top a => NP RT a -> [Text]
productColumns = hcollapse . hmap (K . rtColumn)

-- | collect columns from an element
recordColumns :: L op (RC RT) o a -> [Text]
recordColumns E = []
recordColumns (L (RC f) rest) = productColumns f <> recordColumns rest
recordColumns (T f rest) = rtColumn f : recordColumns rest

--------------   L handy composition operators -----------------------
li :: ( IsRecord x) => x -> L op I o b -> L ('PutOp op) I o (x : b)
li = L . I

lie :: (IsRecord x ) => x -> L ('PutOp 'NoOp) I o '[x]
lie x = li x E

ti :: (IsUpdateOp op ~ False) => x -> L op I x b -> L ('UpdateOp op) I x b
ti = T . I

tie ::  x -> L DeleteOp I x '[]
tie x = ti x E

-- | L like operator grouping right
(++:) :: (IsRecord a) => f a -> L op f o b -> L ('PutOp op) f o (a : b)
(++:) = L

-- | shortcut to compose last 2 elements
(++^)
  :: (IsRecord a1,  IsRecord a2)
  => f a2
  -> f a1
  -> L ('PutOp ('PutOp 'NoOp)) f o '[a2, a1]
(++^) x y = L x $ L y E

infixr 0 ++:

infixr 0 ++^

-- | a put 'L' element type
type LP f o x = L (PutOpOf x) f o x

-- | an update 'L' element type
type LU f o x = L ('UpdateOp (PutOpOf x)) f o x

-- | a delete 'L' element type
type LD f o = L DeleteOp f o '[]

getP1 :: forall x k. L ('UpdateOp ('PutOp 'NoOp)) I k '[x] -> x
getP1 (T _ (L (I x) E)) = x

getU1 :: forall x k. L ('PutOp 'NoOp) I k '[x] -> x
getU1 (L (I x) E) = x

getU2 :: forall x y k. L ('PutOp ('PutOp 'NoOp)) I k '[x, y] -> (x, y)
getU2 (L (I x) (L (I y) E)) = (x, y)

getT1 :: forall x k. L ('UpdateOp ('PutOp 'NoOp)) I k '[x] -> (k, x)
getT1 (T (I t) (L (I x) E)) = (t, x)

getD :: LD I o -> o
getD (T (I t) E) = t

getT2 :: forall x y k. L ('UpdateOp ('PutOp ('PutOp 'NoOp))) I k '[x, y] -> (k, x, y)
getT2 (T (I t) (L (I x) (L (I y) E))) = (t, x, y)

lieFromProduct :: (ToLie xs, IsProductType a xs) => a -> L (PutOpOf xs) I index xs
lieFromProduct = toLie . productTypeFrom

tieFromProduct
  :: ( ToLie xs
     , IsProductType a xs

     , IsUpdateOp (PutOpOf xs) ~ False
     )
  => (index, a)
  -> L (UpdateOpOf xs) I index xs
tieFromProduct (q, x) = ti q $ lieFromProduct x

lieToProduct :: (FromLie xs, IsProductType a xs) => L (PutOpOf xs) I index xs -> a
lieToProduct = productTypeTo . fromLie

tieToProduct
  :: ( FromLie xs
     , IsProductType a xs

     , IsUpdateOp (PutOpOf xs) ~ False
     )
  => L (UpdateOpOf xs) I index xs
  -> (index, a)
tieToProduct (T (I q) l) = (q, lieToProduct l)

class ToLie xs where
  toLie :: forall index. NP I xs -> L (PutOpOf xs) I index xs

instance ToLie '[] where
  toLie Nil = E

instance (ToLie xs, IsProductType x ys) => ToLie (x ': xs) where
  toLie (I r :* rs) = L (I r) $ toLie rs

class FromLie xs where
  fromLie :: forall index. L (PutOpOf xs) I index xs -> NP I xs

instance FromLie '[] where
  fromLie _ = Nil

instance (FromLie xs, IsProductType x ys) => FromLie (x ': xs) where
  fromLie (L (I r) rs) = I r :* fromLie rs
